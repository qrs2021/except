package qrs2021.except;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.beust.jcommander.JCommander;

import qrs2021.except.generator.RepairTargetGenerator;
import qrs2021.except.model.FailureInfo;
import qrs2021.except.model.RepairTarget;
import qrs2021.except.util.ExceptParametersCommander;
import qrs2021.except.util.SbflRankMergingUtil;
import qrs2021.except.util.TestSuiteExecutor;

public class App {
	
	private static final String APP_NAME = "Except";
	
    public static void main( String[] args ) {
    	
    	ExceptParametersCommander exceptParametersCommander = new ExceptParametersCommander();
		
		JCommander jCommander = JCommander.newBuilder().addObject(exceptParametersCommander).build();
		jCommander.setProgramName(APP_NAME);
		jCommander.parse(args);
		
		if (exceptParametersCommander.isHelp()) {
        	jCommander.usage();
		}
		
		String programSourceCodePath = exceptParametersCommander.getSourceCodePath();
		String programClassesPath = exceptParametersCommander.getBuildClassesPath();
    	String testClassesPath = exceptParametersCommander.getBuildTestClassesPath();
    	String classTestToExecute = exceptParametersCommander.getFailingTestClass();
    	String failingTestCase = exceptParametersCommander.getFailingTestName();
    	String dependencies = exceptParametersCommander.getDependencies();
    	String sbflRankFile = exceptParametersCommander.getSbflRanking();
    	String output = exceptParametersCommander.getOutputDirectory();
    	
    	System.out.println("Analysis started...");
    	
    	List<FailureInfo> failureInfoList = 
    			TestSuiteExecutor.getFailure(programClassesPath, testClassesPath, dependencies, classTestToExecute, failingTestCase);
    	
    	Map<FailureInfo, List<RepairTarget>> repairTargetByFailureInfo = new HashMap<FailureInfo, List<RepairTarget>>();
    	
    	for (FailureInfo failureInfo : failureInfoList) {
    		List<RepairTarget> repairTargetList = RepairTargetGenerator.generateRepairTarget(failureInfo, programSourceCodePath);
    		
    		System.out.println("Repair Target list: " + repairTargetList);
    		
    		repairTargetByFailureInfo.put(failureInfo, repairTargetList);
    	}
    	
    	List<RepairTarget> repairTargetList2 = repairTargetByFailureInfo.get(failureInfoList.get(0));
    	
    	SbflRankMergingUtil.merge(repairTargetList2, sbflRankFile, output);
    	
    	System.out.println("Process ended: the output is available in the folder " + output);
    }
}
