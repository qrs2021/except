package qrs2021.except.failure_localizer;

import java.util.List;

import qrs2021.except.model.FailureInfo;
import qrs2021.except.model.SuspiciousLocation;
import spoon.Launcher;

public abstract class FailureDrivenLocalizer {
	
	protected Launcher launcher;
	
	public FailureDrivenLocalizer(String programSourceCodePath) {
		launcher = new Launcher();
		launcher.addInputResource(programSourceCodePath);
		launcher.getEnvironment().setCommentEnabled(false);
		launcher.buildModel();
	}
	
	public abstract List<SuspiciousLocation> getSuspiciousLocations(FailureInfo failureInfo);
}
