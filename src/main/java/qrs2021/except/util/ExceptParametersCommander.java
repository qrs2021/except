package qrs2021.except.util;

import com.beust.jcommander.Parameter;

public class ExceptParametersCommander {
	
	@Parameter(names={"--sourceCodePath", "-s"}, required = true,
			description = "It specifies the path associated with the program source code path.")
	
	private String sourceCodePath;
	
	@Parameter(names={"--buildClassesPath", "-bc"}, required = true,
	description = "It specifies path associated with the compiled classes of the program.")
	
	private String buildClassesPath;
	
	@Parameter(names={"--buildTestClassesPath", "-bt"}, required = true,
	description = "It specifies path associated with the compiled test classes of the program.")
	
	private String buildTestClassesPath;
	
	@Parameter(names={"--failingTestClass", "-ftc"}, required = true,
	description = "It specifies the class that contains the failing test case to be executed.")
	
	private String failingTestClass;
	
	@Parameter(names={"--failingTestName", "-ftn"}, required = true,
	description = "It specifies the name of the failing test case to be executed.")
	
	private String failingTestName;
	
	@Parameter(names={"--dependencies", "-d"},
	description = "It specifies the dependencies required by the program.")

	private String dependencies;
	
	@Parameter(names={"--outputDirectory", "-o"}, required = true, 
			description = "The directory where to save the results.")
	
	private String outputDirectory;
	
	@Parameter(names={"--sbflRanking", "-r"}, required = true, 
			description = "The file that contains the SBFL ranking values.")
	
	private String sbflRanking;
	
	@Parameter(names={"--help"}, help = true)
    private boolean help;

	public String getSourceCodePath() {
		return sourceCodePath;
	}

	public String getBuildClassesPath() {
		return buildClassesPath;
	}

	public String getBuildTestClassesPath() {
		return buildTestClassesPath;
	}

	public String getFailingTestClass() {
		return failingTestClass;
	}

	public String getFailingTestName() {
		return failingTestName;
	}

	public String getDependencies() {
		return dependencies;
	}

	public String getOutputDirectory() {
		return outputDirectory;
	}
	
	public String getSbflRanking() {
		return sbflRanking;
	}

	public boolean isHelp() {
		return help;
	}
}
